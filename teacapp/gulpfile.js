var gulp = require('gulp');
var sass = require('gulp-sass');
var coffee = require('gulp-coffee');
var gutil = require('gulp-util');

gulp.task('styles', function() {
    gulp.src('css/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'));
});

gulp.task('coffee', function() {
  gulp.src('./js/*.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest('./js/'));
});

gulp.task('default',function() {
    gulp.watch('css/**/*.scss',['styles']);
    gulp.watch('./js/*.coffee',['coffee']);

});
