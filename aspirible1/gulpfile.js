var gulp = require('gulp'), sass = require('gulp-sass');
var gutil = require('gulp-util');


var sassConfig = {
	inputDirectory: 'scss/*.scss',
	outputDirectory: 'css',
	options: {
		outputStyle: 'expanded'
	}
}

gulp.task('build-css', function() {
	return gulp
		.src(sassConfig.inputDirectory)
		.pipe(sass(sassConfig.options).on('error', sass.logError))
		.pipe(gulp.dest(sassConfig.outputDirectory))
});


gulp.task('watch', function() {
	gulp.watch('scss/*.scss', ['build-css']);
});
